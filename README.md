# Handbook Korama

1st of all, đây là handbooks của Korama :( Nó sẽ có tất tần tật liên quan đến rules, roles và ti tỉ thứ khác hay ho mà bọn khách hàng mong muốn như cách chạy code

#### Fact:

Đây là cái repo duy nhất sẽ không bị set quyền branch

#### Hướng dẫn

[Lưu ý khi code trên Windows](https://bitbucket.org/korama/handbook/src/master/Windows.md)

[Chạy Project](https://bitbucket.org/korama/handbook/src/master/Project.md)

[Task và nhận task](https://bitbucket.org/korama/handbook/src/master/Task.md)

[Commit code và tạo branch làm task](https://bitbucket.org/korama/handbook/src/master/Commit.md)

[Contact](https://bitbucket.org/korama/handbook/src/master/Contact.md)