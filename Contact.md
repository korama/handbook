# Contact

Hiện tại Korama đang sử dụng Messenger, Slack làm 2 kênh liên hệ chính :(

- [Slack](https://join.slack.com/t/koramateam/shared_invite/enQtOTU2NzUzMzgzMDQ2LTdhNmRhNTNhMjVmYTMzMWMzZjY5NDY3MzMzNzQ5ZTFhZDcyOWI0ZDA3ZTQ5OWI5YjA3MmNiMjRmY2QyYzEzMzE)

- Messenger: Thường được add vào khi vào team luôn :( nến không dẫn link nữa

Ngoài ra, Korama hiện tại đang quản lí công việc qua Jira, lưu trữ trên bitbucket

- [Jira](http://korama.atlassian.net/)
- [Bitbucket](https://bitbucket.org/%7Bde046ff6-ed05-4e6c-af3d-92adcddf1961%7D/)

- [Contact list](https://docs.google.com/spreadsheets/d/1ZwbQ7ItaHdC5Sbi0l6PSHyTyZBAtOVTP1IcTf9Aqf-U/edit?usp=sharing)

:( Anh em nào mới vào tự join slack, để lại contact trong contact list rồi ping inbox nhờ add vào Jira và bitbucket nhé 