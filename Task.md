# Task

Task của Korama sẽ được phân tại [Backlog trên Jira](korama.atlassian.net/jira/software/projects/KOR/boards/1/backlog) của team kèm theo các task với các points

### Hướng dẫn nhận task

#### Step 1: Truy cập backlog của team

Truy cập vào backlog của team trên Jira [tại đây](korama.atlassian.net/jira/software/projects/KOR/boards/1/backlog). Giao diện sẽ như bên dưới
![](https://bitbucket.org/korama/handbook/raw/da9599c115afe4abcc388ce3b18361769db889a5/images/task/listtask.png)
Chọn lấy 1 task phù hợp với bạn

#### Step 2: Set status

Có 3 trạng thái status chính
![](https://bitbucket.org/korama/handbook/raw/efad27a5aba805ecfddf5b01523b005078af0639/images/task/status.png)

- To Do: Là những task chưa làm, task vẫn trong trạng thái có thể nhận. Nếu task được assign hãy comment hoặc inbox với người được assign để chuyển task. Tránh nhận lung tung :D
- In progess: Những task đang làm
- Done: Những task đã làm xong. Chỉ có người reporter mới được set task Done. Nếu như assignee làm xong có thể inbox, comment dưới task để team review lại trước khi pick done

#### Step 3: Set assignee

Assignee sẽ là người chịu trách nhiệm chính về task đó và thường là bạn hãy assign chính mình
![](https://bitbucket.org/korama/handbook/raw/da9599c115afe4abcc388ce3b18361769db889a5/images/task/assign.png)
Trong trường hợp đó là 1 task (gồm nhiều child task). Nếu bạn làm hết tất cả hoặc chịu trách nhiệm chính về task hãy set assignee mình vào và set Related những người có liên quan ở ngay bên dưới

#### Step 4: Set estimate points

Points sẽ được quy định bằng một giờ đối với mức trung bình của Team. Points là một mức điểm đánh giá khối lượng công việc của bạn với 1 projects

Giả sử công việc đó với tiến độ trung của Project là 3 points (Như dựng môi trường,...) nhưng nếu bạn làm được trong 1 tiếng thì bạn vẫn ăn lương 3 tiếng :D

Nếu task chưa được estimate points thì team bạn cần estimate points và team sẽ review lại khi duyệt task đó và set done

![](https://bitbucket.org/korama/handbook/raw/efad27a5aba805ecfddf5b01523b005078af0639/images/task/point-est.png)
