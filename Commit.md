# Commit code và branch

Trước hết, chỉ có admin mới có quyền được push vào branch master, nên mọi người cần phải tạo các branch riêng khi muốn làm task qua Jira

### Hướng dẫn

##### Step 1: Nhận task

Vui lòng xem thêm về Task tại [đây](https://bitbucket.org/korama/handbook/src/master/Task.md)

#### Step 2: Tạo branch mới

Chọn `Create Branch` ở gần dưới của task
![](https://bitbucket.org/korama/handbook/raw/448795104b6ad87230657efa653fec08d0969a43/images/commit/new-branch.png)

Một cửa sổ khác được mở ra để chọn thuộc tính branch mới

- Repository: Lựa chọn repo phù hợp với branch
  ![](https://bitbucket.org/korama/handbook/raw/448795104b6ad87230657efa653fec08d0969a43/images/commit/fromrepo.png)
  Ở đây mình đang chọn task liên quan đến frontend nên mình chọn rudias/front-end

- Type: Loại của branch. Thường sẽ được quy định trên task.
  ![](https://bitbucket.org/korama/handbook/raw/448795104b6ad87230657efa653fec08d0969a43/images/commit/type.png)

- From branch: Ở đây thường sẽ lấy từ bản release mới nhất (master). Trong một số trường hợp cụ thể sẽ được ghi ở dưới description của task.
  ![](https://bitbucket.org/korama/handbook/raw/448795104b6ad87230657efa653fec08d0969a43/images/commit/from-branch.png)
