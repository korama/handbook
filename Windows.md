# Lưu ý khi code trên Windows

Tuy sử dụng Docker nhưng nhiều khi không tránh những lỗi khi code. Vậy nên, khi sử dụng Windows cần lưu ý một số vấn đề sau

### EOL (End of line)

Vì environment deploy là server ubuntu vậy nên EOL của tất cả project sẽ là LF (\n)
Khi sử dụng sang Windows, mặc định sẽ là CRLF, ta cần chuyển lại sang LF để có thể đảm bảo code chạy ổn nhất

Đây là chỗ chuyển EOL của 1 file trên VSCode.
![](https://bitbucket.org/korama/handbook/raw/5550d03e7c2c19ad25efefad92b5a5b7a87b160a/images/code/eolchange.png)

Để set mặc định EOL trên mỗi khi clone repo ta sử dụng lệnh sau

```bash
git config --global core.eol lf
```

Vào VSCode, nhấn tổ hợp `Ctrl + ,` chuyển EOL sang `\n`
![](https://bitbucket.org/korama/handbook/raw/93242cae32e0898171acd4f0e9962334755e4e48/images/code/eolvscode.png)

