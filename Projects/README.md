# List các project của Korama

- [Rudias Blog](https://bitbucket.org/korama/handbook/src/master/project/rudias-blog/): Blog giới thiệu về Rudias Outsource (rudias-blog)

- [Callmeduy](https://bitbucket.org/korama/handbook/src/master/project/callmeduy-blog/): Blog giới thiệu về Beauty Blogger Vũ Duy (callmeduy-blog)

# Hướng dẫn chung

Để chạy mỗi Project thì bạn cần clone repo [handbook](https://bitbucket.org/korama/) và repo [server](https://bitbucket.org/korama/docker-server) của Korama. Hãy đảm bảo server được clone 2 repo này trước khi chạy
