# Rudias Blog

## Mô tả

Đây là repo về blog của Rudias

## Công nghệ sử dụng

- [Docker CE](https://www.docker.com/)
- [NodeJS 12.16.0](https://nodejs.org/)
- [NginX](https://www.nginx.com/)
- [MongoDb](https://www.mongodb.com/)
- [KeystoneJS v5 CMS](https://www.keystonejs.com/)
- [NextJS](https://nextjs.org/)

## List repo [Rudias](https://bitbucket.org/account/user/korama/projects/RUD)

- [Rudias Keystone](https://bitbucket.org/korama/rudias-keystone)
- [Rudias Frontend](https://bitbucket.org/korama/rudias-frontend)

## Hướng dẫn chạy project

#### Trên môi trường Local (Development)

- Hãy đảm bảo clone đủ 2 repo được nói đến trong [Hướng dẫn chung](https://bitbucket.org/korama/handbook/src/master/Projects/README.md)

- Chạy lần lượt những câu lệnh sau

```shell
cp handbook/projects/rudias-blog/config docker-server/ -r

docker-compose -f rudias-frontend/docker-compose.dev.yml up -d

docker-compose -f rudias-keystone/docker-compose.dev.yml up -d

docker-compose -f docker-server/docker-compose.dev.yml up -d

```

## Contact

Mọi issue được tiếp nhận qua [Jira](korama.atlassian.net/jira/software/projects/KOR/boards) tại Rudias :D
