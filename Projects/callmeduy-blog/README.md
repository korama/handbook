# Rudias Blog

## Mô tả

Đây là repo về blog giới thiệu về Beauty Blogger Vũ Duy

## Công nghệ sử dụng

- [Docker CE](https://www.docker.com/)
- [NodeJS 12.16.0](https://nodejs.org/)
- [NginX](https://www.nginx.com/)
- [Strapi CMS](http://strapi.io/)
- [PostgressSQL](https://www.postgresql.org/)
- [NextJS](https://nextjs.org/)

## List repo [Rudias](https://bitbucket.org/account/user/korama/projects/CMD)

- [Callmeduy Strapi](https://bitbucket.org/korama/cmd-strapi)
- [Callmeduy Owner](https://bitbucket.org/korama/cmd-owner)
- [Callmeduy Client](https://bitbucket.org/korama/cmd-client)

## Hướng dẫn chạy project

#### Trên môi trường Local (Development)

- Hãy đảm bảo clone đủ 2 repo được nói đến trong [Hướng dẫn chung](https://bitbucket.org/korama/handbook/src/master/Projects/README.md)

- Chạy lần lượt những câu lệnh sau

```shell
cp handbook/projects/callmeduy-blog/config docker-server/ -r

docker-compose -f cmd-strapi/docker-compose.dev.yml up -d

docker-compose -f cmd-owner/docker-compose.dev.yml up -d

docker-compose -f cmd-clien/docker-compose.dev.yml up -d



```

## Contact

Mọi issue được tiếp nhận qua [Jira](korama.atlassian.net/jira/software/projects/KOR/boards) tại Rudias :D
